<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/student/{id?}', function ($id=null) {
    if($id!=null)
    return "hello student ".$id;
    else
    return "no student number was provided";
});

Route::get('/comment/{id?}', function ($id) {
    return view('comment',['id'=>$id]); 
})->name('comments');

Route::get('/customer/{id?}', function ($id) {
    return view('customer', ['id'=>$id]);
})->name('customers');

Route::resource('todos', 'TodoController')->middleware('auth');

//Route::resource('todos', 'BookController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
