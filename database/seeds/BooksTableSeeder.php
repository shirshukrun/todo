<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                        'title' => 'rush hour 1',
                        'author' => 1,
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'title' => 'rush hour 2',
                        'author' => 1,
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        'title' => 'rush hour 3',
                        'author' => 1,
                        'created_at' => date('Y-m-d G:i:s'),
                ],
            
                    ]);
    }
}
